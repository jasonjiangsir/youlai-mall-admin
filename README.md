[![](https://img.shields.io/badge/Author-有来开源组织-orange.svg)](https://gitee.com/wangjiabin-x/uh5)
![](https://img.shields.io/badge/youlai--mall-v2.0.0-blue)
[![](https://img.shields.io/github/stars/hxrui/youlai-mall.svg?style=social&label=Stars)](https://github.com/hxrui/youlai-mall/stargazers)
[![](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)](https://github.com/hxrui/youlai-mall/blob/master/LICENSE)
![](https://img.shields.io/badge/SpringBoot-2.6.3-brightgreen.svg)
![](https://img.shields.io/badge/SpringCloud-2021-green.svg)
![](https://img.shields.io/badge/vue3--element--admin-v1.0.0-orange)

**线上预览地址：** http://www.youlai.tech

#### 项目预览

| ![image-20210621004954228](https://gitee.com/haoxr/image/raw/master/image-20210621004954228.png) | ![image-20210621005011310](https://gitee.com/haoxr/image/raw/master/image-20210621005011310.png) |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
|![](https://gitee.com/haoxr/image/raw/master/30719657a4b183428a2472231fee55a6_image-20210621005037964.png) | ![image-20210621005123432](https://gitee.com/haoxr/image/raw/master/image-20210621005123432.png) |


#### 技术栈

- **前端技术栈：** vue3、vite2、element-plus、typescript、vue-element-admin


#### 项目地址

| 项目名称   | 源码地址                                                        |项目名称   | 源码地址                                                   |
| ---------- | ------------------------------------------------------------ |---------- | ------------------------------------------------------------ |
| 微服务后台 | [youlai-mall](https://gitee.com/youlaitech/youlai-mall)      | 微信小程序 | [youlai-mall-weapp](https://gitee.com/youlaitech/youlai-mall-weapp) |
| 管理前端   | [youlai-mall-admin](https://gitee.com/youlaitech/youlai-mall-admin) |APP应用    | [youlai-mall-app](https://gitee.com/youlaitech/youlai-mall-app) |


#### 项目启动

1. 本机安装Node环境
2. npm install
3. npm run dev
4. 访问 http://localhost:9527

#### 项目文档

[项目文档地址](https://www.cnblogs.com/haoxianrui/)


#### 联系信息
因为微信交流群满200人只能通过邀请进入，如果想进入交流群学习可添加以下开发人员，备注“**有来**“由其拉进群。


| ![](https://gitee.com/haoxr/image/raw/master/default/113__6c5ed5b1b73ea9cd4cf32848ed350c07_b9b214638a2a406e52dbf51e9bf9a2ef.png) | ![](https://gitee.com/haoxr/image/raw/master/hxr.jpg) | ![](https://gitee.com/haoxr/image/raw/master/default/20210701234946.png) | ![](https://gitee.com/haoxr/image/raw/master/default/image-20210702002909113.png) |
| ------------------------------------------------------------ | ----------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |

